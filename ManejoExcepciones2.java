package manejoexcepciones2;

import datos.*;
import excepciones.*;


/**
 *
 * @author tinix
 */
public class ManejoExcepciones2 {

    public static void main(String[] args) {
        AccesoDatos datos = new ImplementacionMySql();
        //Cambiamos el estado a simularError = true
        datos.simularError(true);
        ejecutar(datos, "listar");
        
        datos.simularError(false);
        System.out.println("");
        ejecutar(datos, "insertar");
      
    }
    
    public static void ejecutar(AccesoDatos datos , String accion) {
        if ("listar".equals(accion)) {
            try{
                datos.listar();
            } 
            
            catch (LecturaDatosEx ex) {
                System.out.println("Error lectura : Procesa la excepcion mas especifica de lectura de datos");
            } 
            
            catch (AccesoDatosEx ex) {
                System.out.println("Error Acceso datos :  Procesa la excepcion mas generica de lectura de datos ");
            } 
            
            catch (Exception ex) {
                System.out.println("Error general");
            }
            finally {
                System.out.println("Procesar finally es opcional , siempre se ejecutara son importar si hubo error o no ");
            }
        } else if ("insertar".equals(accion)) {
            try { 
                datos.insertar();
            } catch (AccesoDatosEx ex) {
                System.out.println("Error acceso datos : Podemos proocesar solo la excepcion mas generica");
            }
            finally{
                System.out.println("Procesar finally es opcional , siempre se ejecutara son importar su hubo error o no");
            }
        }
        else
            System.out.println("No se proporciono ninguna accikon conocida");
    }
}
